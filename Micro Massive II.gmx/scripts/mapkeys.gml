keyboard_unset_map()

keyboard_set_map(ord('A'),vk_left)
keyboard_set_map(ord('W'),vk_up)
keyboard_set_map(ord('D'),vk_right)
keyboard_set_map(ord('S'),vk_down)
if argument[0] = true
keyboard_set_map(vk_space,vk_up)
// Azerty support
keyboard_set_map(ord('Q'),vk_left)
keyboard_set_map(ord('Z'),vk_up)

// Pause menu
keyboard_set_map(vk_enter,vk_space)
keyboard_set_map(vk_escape,vk_backspace)
