//Heavily based on BBGamings 'Moving Platforms' example: http://gmc.yoyogames.com/index.php?showtopic=488628

//Moves the platform, and moves the player when he's in the way. If something is really in the way, we just stop our move altogether.
//xx and yy are the current coords of your platform, and if bypass is true the platform may move through others.

var xx,yy,bypass;
xx=argument0; yy=argument1; bypass=argument2
xprev=o_player.x; yprev=o_player.y; stop=false; movechar=false

//Verplaatsen van speler als hij op een platform staat of tegen het platform aan staat.
if (place_meeting(x,y-1,o_player))
{ o_player.x+=xx; movechar=true}

if place_meeting(x+xx,y,o_player)
{ if o_player.x>x
   o_player.x=x+xx+(sprite_width+o_player.sprite_width/2)
  else
   o_player.x=x+xx+(sprite_width+o_player.sprite_width/2)*-1
}

if movechar == true
{ o_player.y+=yy }

if place_meeting(x,y+yy,o_player)
{ if o_player.y<y //Als de speler boven het platform is
   o_player.y=y+yy +(0)*-1
  else //Als de speler onder het platform is
   {o_player.y=y+yy+(sprite_height/2+o_player.sprite_height/2)};
}

//Als het platform verplaatst wordt
if !place_free(x+xx,y+yy) && !bypass
{ o_player.x=xprev; o_player.y=yprev; return(1) }

if (abs(o_player.x-xprev)>10 && xx!=0) or (abs(o_player.y-yprev)>10 && yy!=0)
{ o_player.x=xprev; o_player.y=yprev; write_error("Platforming glitch detected!") return(1) }

x+= xx
y+= yy

with(o_player)
{if !place_free(x,y)
 {for(i=1;i<=2;i+=1)
  {if place_free(x,y-i)
   {o_player.y-=i; i=99}
  }
 if i!=100
  {x=other.xprev; y=other.yprev;
   if instance_place(x,y,o_platform_parent)
   other.stop=1
  }
 }
}

if (stop=1 && !bypass)
{ x-=xx; y-=yy; return(1) }

return(0)
