//Ran when it's detected there's no options file. This script will add it, so you can continue without worries.

ini_open("options.ini")
ini_write_real("Options","Fullscreen",false)
ini_write_real("Options","Music",true)
ini_write_real("Options","SoundEffects",0.8)
ini_write_real("Options","GameSpeed",60)
ini_write_real("Help","This game contains option data for Micro Massive 2. 
You can change the options here if you wish, although I'd recommend you to do it from the in-game options menu.
If you ever mess up with the options, delete this file. Next time you play the game, it will be automatically recreated with the deafult options.
WARNING: Changing this file when a quick save is currently available might cause unexpected problems.",0)
ini_write_real("Special","Cheats",0)
ini_write_real("Special","Debug",0)
ini_write_string("Special","Help","You can use the cheats with the function keys. Using cheats might cause unexpected problems.")
ini_close()
write_error("Options.ini successfully created!")
