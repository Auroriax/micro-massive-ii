if !file_exists("options.ini")
{options_init()}
else
{write_error("Options.ini exists. Loading it now...")}
ini_open("options.ini")

if ini_read_real("Options","Fullscreen",1) == 0
window_set_fullscreen(0);

if ini_read_real("Options","Fullscreen",1) == true
window_set_fullscreen(true);

if ini_read_real("Options","Music",1) == 0
sound_stop_all()

if ini_read_real("Special","Debug",0) == 0
o_backbone.visible = false
else
o_backbone.visible = true

sound_rebalance(ini_read_real("Options","SoundEffects",1))
ini_close();
