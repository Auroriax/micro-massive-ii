if global.pauseable == true
{
if global.pause_status == 0
 {
room_persistent = 1
global.pause_status = 1
global.currentroom = room
global.pausebg = background_create_from_surface(application_surface,0,0,1280,1028,0,0)
mapkeys(false)
write_error("Game paused")
sound_play(s_menuselect)
room_goto(rm_pause)
 }
else
 {
transition_kind = 20
transition_steps = 20
global.pause_status = false
mapkeys(true)
write_error("Game unpaused")
background_delete(global.pausebg)
room_goto(global.currentroom)
 }
}
