//Ran when it's detected there's no save file. This script simply creates an empty one.

if file_exists("savefile.ini")
{write_error("Save file found!")}
else
{

var a, i;

ini_open("savefile.ini")

a = rm_flairfair
ini_write_real(string(a),"Record Time",0)
for (i = 1; i <= 1; i += 1)
{ini_write_real(string(a),"Token"+string(i),false)}

a = rm_digital
ini_write_real(string(a),"Record Time",0)
for (i = 1; i <= 3; i += 1)
{ini_write_real(string(a),"Token"+string(i),false)}

a = rm_planets
ini_write_real(string(a),"Record Time",0)
for (i = 1; i <= 3; i += 1)
{ini_write_real(string(a),"Token"+string(i),false)}

a = rm_glitch
ini_write_real(string(a),"Record Time",0)
for (i = 1; i <= 3; i += 1)
{ini_write_real(string(a),"Token"+string(i),false)}

a = rm_intermezzo
ini_write_real(string(a),"Record Time",0)
for (i = 1; i <= 1; i += 1)
{ini_write_real(string(a),"Token"+string(i),false)}

a = rm_appear
ini_write_real(string(a),"Record Time",0)
for (i = 1; i <= 3; i += 1)
{ini_write_real(string(a),"Token"+string(i),false)}

a = rm_appear2
ini_write_real(string(a),"Record Time",0)
for (i = 1; i <= 3; i += 1)
{ini_write_real(string(a),"Token"+string(i),false)}

a = rm_teleport
ini_write_real(string(a),"Record Time",0)
for (i = 1; i <= 3; i += 1)
{ini_write_real(string(a),"Token"+string(i),false)}

a = rm_tutorial
ini_write_real(string(a),"Record Time",0)
for (i = 1; i <= 1; i += 1)
{ini_write_real(string(a),"Token"+string(i),false)}

ini_write_real("Stats","Deaths",0)
ini_write_real("Stats","Jumps",0)
ini_write_real("Stats","Finishes",0)

write_error("No save file detected. We wrote one for you!")
}
