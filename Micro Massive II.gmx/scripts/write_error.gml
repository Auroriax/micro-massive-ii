//Writes errors, including time, to the console log which is displayed on screen.
//Needs a string as argument.
var temp;

global.console += '#'+string(o_backbone.image_index)+" "+string(argument0)

//Here, we also look if the console isn't getting overflooded, since it impacts the game.
if string_length(global.console) >= 1000
{temp = string_pos("#",global.console)
global.console = string_delete(global.console,1,temp)}
